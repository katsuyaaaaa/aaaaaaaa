package com.example.api;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;



import com.example.domain.*;

import com.example.service.CustomerService;

@RestController
@RequestMapping("customerapi")

public class CustomerRestController {
	
	@Autowired
	CustomerService customerService;
	

	
//■■■■■■■■■■■■■■■■■■■■１、ユーザー情報一件取得　　GET型　　URL "{email}/find"■■■■■■■■■■■■■■■
	@RequestMapping(value = "{email}/find",method = RequestMethod.GET)
	Customer userSearch1(@PathVariable String email) {
		Customer customer = customerService.userSearch2(email);
		return customer;
		
	}//1
	
	//検索用URL http://localhost:8080/customerapi/aa@zz.com/find
	
////■■■■■■■■■■■■■■■■■■■■２、ユーザー情報新規登録  　　POST  URL　"save"■■■■■■■■■■■■■■■■■■■■
	@RequestMapping(value = "save",method = RequestMethod.POST)
	CustomerSave userRegistration1(@RequestBody CustomerSave cs ) {  //CustomerSaveクラス作成
		
		CustomerSave returnCs = customerService.userRegistration2(cs.getEmail()
																 ,cs.getPassword()
																 ,cs.getNickname()
																 ,cs.getAge());
		return returnCs;
	}//2
//
//	//検索用URL http://localhost:8080/customerapi/save
//	
//■■■■■■■■■■■■■■■■■■■■３、ログイン  　　POST型    URL　"login"■■■■■■■■■■■■■■■■■■■■■
	@RequestMapping(value = "login",method = RequestMethod.POST)
	CustomerLogInAndLogOut loginUser1(@RequestBody CustomerLogInAndLogOut cl ) {  
		CustomerLogInAndLogOut returnCl = customerService.loginUser2(cl.getEmail()  ,cl.getPassword());
		
		return returnCl;
		
	}//3
	
	//検索用URL http://localhost:8080/customerapi/login
	
//■■■■■■■■■■■■■■■■■■■■４、ログアウト  　　POST型    URL "logout"■■■■■■■■■■■■■■■■■■■■
	@RequestMapping(value = "logout",method = RequestMethod.POST)
	CustomerLogInAndLogOut logout1(@RequestBody CustomerLogInAndLogOut cl ) { 
		CustomerLogInAndLogOut returnCl = customerService.logout2(cl.getEmail()  ,cl.getPassword());
		return returnCl;
	}//4
	
	//検索用URL http://localhost:8080/customerapi/logout
//	
//■■■■■■■■■■■■■■■■■■■■５、フレンド全件取得　　GET型    URL"{email}/friend"■■■■■■■■■■■■■■■■■■■■
	@RequestMapping(value = "{email}/friend",method = RequestMethod.GET)
	List<CustomerFriend> friendSearch1(@PathVariable  String email) { //CustomerFriendクラス作成
		List<CustomerFriend> listCf = customerService.friendSearch2(email);
		return listCf;
		
	}//5 

	//検索用URL http://localhost:8080/customerapi/aa@zz.com/friend

//■■■■■■■■■■■■■■■■■■■■６、フレンド一件登録　　GET型    URL"{email}/{friendEmail}/friend"■■■■■■■■■■■■■■■■■■■■
	@RequestMapping(value = "{email}/{friendEmail}/friend",method = RequestMethod.GET)
	List<CustomerFriend> friendRegistration1(@PathVariable String email
											,@PathVariable String friendEmail) {
		List<CustomerFriend> listCf = customerService.friendRegistration2(email ,friendEmail );
		return listCf;
		
	}//6
	
	//検索用URL  http://localhost:8080/customerapi/cc@zz.com/bb@zz.com/friend
	
//■■■■■■■■■■■■■■■■７、フレンド一件削除　　GET型    URL"{email}/{friendEmail}/deleteFriend"■■■■■■■■■■■■■■■■
	@RequestMapping(value = "{email}/{friendEmail}/deleteFriend",method = RequestMethod.GET)
	CustomerFriend friendDelete1(@PathVariable String email
						  ,@PathVariable String friendEmail) {
		CustomerFriend returnCf = customerService.friendDelete2( email,friendEmail);
		return returnCf;
		
	}//7
	
	//検索用URL	http://localhost:8080/customerapi/cc@zz.com/bb@zz.com/deleteFriend
	
//■■■■■■■■■■■■■■■■■■■■８、所持モンスター全件取得　　GET型    URL"{email}/monster"■■■■■■■■■■■■■■■■■■■■
	@RequestMapping(value = "{email}/monster",method = RequestMethod.GET)
	String monsterSearch1(@PathVariable String email) {
		String monsterNames = customerService.monsterSearch2(email);
		return monsterNames;
		
	}//8
	
	//検索用URL	http://localhost:8080/customerapi/bb@zz.com/monster 
	
//■■■■■■■■■■■■■■■■■■■■９、ポイント加算　　GET型     URL"{email}/{lvNum}/point"■■■■■■■■■■■■■■■■■■■■
	@RequestMapping(value = "{email}/{point}/point",method = RequestMethod.GET)
	Customer pointAddition1(@PathVariable String email,@PathVariable int point) {
		Customer customer = customerService.userSearch2(email,point);
		return customer;
		
	}//9
	
	//検索用URL	http://localhost:8080/customerapi/bb@zz.com/100/point
	
//■■■■■■■■■■■■■■■■■■■■１0、ログインユーザー情報一件取得　　GET型　　URL "{email}/loginuser"■■■■■■■■■■■■■■■
	@RequestMapping(value = "{email}/loginuser",method = RequestMethod.GET)
	LoginUser userLogin1(@PathVariable String email) {
		LoginUser loginUser = customerService.userLogin2(email);
		return loginUser;
			
}//10
		
		//検索用URL http://localhost:8080/customerapi/aa@zz.com/find
		
}//class

