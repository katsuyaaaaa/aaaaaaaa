package com.example.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.example.domain.*;
import com.example.repository.*;

import java.util.ArrayList;   //アレイリストをインポート
import java.util.List;        //リストをインポート

@Service

public class CustomerService {
	
	
	
	
//■■■■■■■■■■■■■■■■■■■■１、ユーザー情報一件取得　　GET型　　URL "{email}/find"■■■■■■■■■■■■■■■
	@Autowired
	NickNameRepository1 nickNameRepository1;
	
	public Customer userSearch2(String email) {
		return nickNameRepository1.userSearch3(email);
	}//1
	
//■■■■■■■■■■■■■■■■■■■■２、ユーザー情報新規登録  　　POST  URL　"save"■■■■■■■■■■■■■■■■■■■■
	
	@Autowired
	No2 no2;
	public CustomerSave userRegistration2(String Email,String Password,String Nickname,int Age) {
		return no2.userRegistration3(Email,Password,Nickname,Age);
			
	}//3	

//■■■■■■■■■■■■■■■■■■■■3、ユーザーのログイン処理　　POST型　　URL "login"■■■■■■■■■■■■■■■
		
	@Autowired
	No3 no3;
	public CustomerLogInAndLogOut loginUser2(String email,String password) {
		return no3.loginUser3(email,password);
			
	}//3	

// ■■■■■■■■■■■■■■■■■■■■4、ログアウト POST型 URL "/logout"■■■■■■■■■■■■■■■
	
	@Autowired
	No4 no4;
	public CustomerLogInAndLogOut logout2(String email,String password) {
	return no4.logout3(email,password);
	}//4
	
//■■■■■■■■■■■■■■■■■■■■５、フレンド全件取得　　GET型    URL"{email}/friend"■■■■■■■■■■■■■■■■■■■■
	
	@Autowired
	No5 no5;
	public List<CustomerFriend> friendSearch2(String email){
		return no5.friendSearch3(email);
	}
	
	
//■■■■■■■■■■■■■■■■■■■■６、フレンド一件登録　　GET型    URL"{email}/{friendEmail}/friend"■■■■■■■■■■■■■■■■■■■■
	
	@Autowired
	No6 no6;
	public List<CustomerFriend> friendRegistration2(String email , String friendEmail) {
		return no6.friendRegistration3(email , friendEmail);
	}//6

//■■■■■■■■■■■■■■■■７、フレンド一件削除　　GET型    URL"{email}/{friendEmail}/deleteFriend"■■■■■■■■■■■■■■■■
	
	@Autowired
	No7 no7;
	public CustomerFriend friendDelete2(String email , String friendEmail) {
		return no7.friendDelete3(email , friendEmail);
	}//7
	
	
	
//■■■■■■■■■■■■■■■■■■■■８、所持モンスター全件取得　　GET型    URL"{email}/monster"■■■■■■■■■■■■■■■■■■■■
	@Autowired
	No8 no8;
	public String monsterSearch2(String email) {
		return no8.monsterSearch3(email);
	}//8
	
	
	
//■■■■■■■■■■■■■■■■■■■■９、ポイント加算　　GET型     URL"{email}/{lvNum}/point"■■■■■■■■■■■■■■■■■■■■
	@Autowired
	No9 no9;
	public Customer userSearch2(String email, int point) {
		return no9.userSearch3(email, point);
	}//9

//■■■■■■■■■■■■■■■■■■■■１0、ログインユーザー情報一件取得　　GET型　　URL "{email}/loginuser"■■■■■■■■■■■■■■■
	@Autowired
	No10 no10;
		
	public LoginUser userLogin2(String email) {
	return no10.userLogin3(email);
}//1
		
}
