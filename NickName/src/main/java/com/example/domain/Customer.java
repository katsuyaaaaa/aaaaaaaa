package com.example.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor

public class Customer {
	private String email;
	private String password;
	private String nickname;
	private int    point;
	private int    rank;
	private int    age;
	private String entryDate;
	private String lastDateTime;
	private int    loginState;
}


