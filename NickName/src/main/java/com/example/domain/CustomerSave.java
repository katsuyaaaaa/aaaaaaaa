package com.example.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor

@Data
//`Email`, `Password`, `NickName`, `Point`, 
//`Rank`, `Age`, `EntryDate`, `LastDateTime`, `LoginState

public class CustomerSave {
	private String email;
	private String password;
	private String nickname;
	private int    age;
}
