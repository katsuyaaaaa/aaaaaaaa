package com.example.domain;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CustomerLogInAndLogOut {
	private String email;
	private String password;
	private String nickname;
	private String lastDateTime;
	private int    loginState;
	
}
