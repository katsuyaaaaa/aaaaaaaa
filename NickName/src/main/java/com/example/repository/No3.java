package com.example.repository;

import java.text.SimpleDateFormat;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.example.domain.CustomerLogInAndLogOut;
import com.example.domain.CustomerSave;

@Repository
@Transactional

public class No3 {
	@Autowired
	NamedParameterJdbcTemplate jdbcTemplate;

	CustomerLogInAndLogOut c;

	private static final RowMapper<CustomerLogInAndLogOut> customerRowMapper = (rs, i) -> {
		String email = rs.getString("Email");
		String password = rs.getString("Password");
		String nickName = rs.getString("NickName");
		String lastDateTime = new SimpleDateFormat("yyyy/MM/dd").format(rs.getDate("LastDateTime"));
		int loginState = rs.getInt("LoginState");

		return new CustomerLogInAndLogOut(email, password, nickName, lastDateTime, loginState);
	};

	public CustomerLogInAndLogOut loginUser3(String email, String password) {
		SqlParameterSource param = new MapSqlParameterSource().addValue("email", email).addValue("password", password);

		try {
			c = jdbcTemplate.queryForObject("SELECT * FROM user_basic WHERE Email = :email AND Password = :password",
					param, customerRowMapper);

			if (c.getLoginState() == 0) {
				jdbcTemplate.update(
						"UPDATE user_basic SET loginState = 1 WHERE Email = :email AND Password = :password", param);
				c = jdbcTemplate.queryForObject(
						"SELECT * FROM user_basic WHERE Email = :email AND Password = :password", param,
						customerRowMapper);
			} else if (c.getLoginState() == 1) {
				System.out.println("すでにログインしています。");
				c = null;
			} else {
				System.out.println("アノニマス");
				c = null;
			}
			return c;

		} catch (EmptyResultDataAccessException e) {
			e.printStackTrace();
			System.out.println("そのアカウントはありません");
			return null;
		}
	}

}
