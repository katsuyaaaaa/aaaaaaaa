package com.example.repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.example.domain.Customer;

@Repository
@Transactional

public class NickNameRepository3 {

	@Autowired
	NamedParameterJdbcTemplate jdbcTemplate;

	Customer c;

	public Customer changeNickName(String email, String nickName) {

		CustomerRowMapper customerRowMapper = new CustomerRowMapper();
		SqlParameterSource param = new MapSqlParameterSource().addValue("email", email).addValue("nickName", nickName);

		try {
			jdbcTemplate.update("UPDATE user_basic SET NickName = :nickName WHERE :email", param);

			c = jdbcTemplate.queryForObject("SELECT * FROM user_basic WHERE Email = :email", param, customerRowMapper);
			return c;
		} catch (EmptyResultDataAccessException e) {
			e.printStackTrace();
			System.out.println("そのアカウントはありません");
			return null;

		}
	}
}

class CustomerRowMapper implements RowMapper<Customer> {
	@Override
	public Customer mapRow(ResultSet rs, int rowNum) throws SQLException {
		String email = rs.getString("Email");
		String password = rs.getString("Password");
		String nickName = rs.getString("NickName");
		int point = rs.getInt("Point");
		int rank = rs.getInt("Rank");
		int age = rs.getInt("Age");
		String entryDate = new SimpleDateFormat("yyyy/MM/dd").format(rs.getDate("EntryDate"));
		String lastDateTime = new SimpleDateFormat("yyyy/MM/dd").format(rs.getDate("LastDateTime"));
		int loginState = rs.getInt("LoginState");

		return new Customer(email, password, nickName, point, rank, age, entryDate, lastDateTime, loginState);
	}
}
