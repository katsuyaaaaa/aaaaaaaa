package com.example.repository;

import java.text.SimpleDateFormat;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.example.domain.Customer;
import com.example.domain.Monster;

@Repository
@Transactional

public  class No8 {

	@Autowired
	NamedParameterJdbcTemplate jdbcTemplate;
	
	
	
	
	private static final RowMapper<Monster> monsterRowMapper = (mo,i) -> {
		String email = mo.getString("Email");
		String Name = mo.getString("Name");
		
		return new Monster(email, Name);
	};
	

//■■■■■■■■■■■■■■■■■■■■８、所持モンスター全件取得　　GET型    URL"{email}/monster"■■■■■■■■■■■■■■■■■■■■
	
	
	public String monsterSearch3(String email) {
		SqlParameterSource param = new MapSqlParameterSource().addValue("email", email) ;
		
		String monsterNames;
		
		Monster mon;
		
		
		try {
			
			mon = jdbcTemplate.queryForObject(
					"SELECT * FROM user_monster WHERE Email = :email "
					, param
					, monsterRowMapper);
			
			
			if( mon != null ) {
				
				
				monsterNames = mon.getName();
				
				return monsterNames;
				
			}else {
				return null;
			}

			
			
		}catch(EmptyResultDataAccessException e) {
			e.printStackTrace();
			System.out.println("そのアカウントはありません");
			return null;
		}
	}
	
}
