package com.example.repository;

import java.text.SimpleDateFormat;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;



import com.example.domain.*;  //ドメインパッケージを全てインポート
import java.util.ArrayList;   //アレイリストをインポート
import java.util.List;        //リストをインポート


@Repository
@Transactional


public class No7 {
	
	@Autowired
	NamedParameterJdbcTemplate jdbcTemplate;
	
	private static final RowMapper<CustomerFriend> customerFriendRowMapper = (cf,i) -> {
		String email = cf.getString("Email");
		String friendemail = cf.getString("friendemail");
		
		
		return new CustomerFriend(email,friendemail);
	};
	
	
//■■■■■■■■■■■■■■■■７、フレンド一件削除　　GET型    URL"{email}/{friendEmail}/deleteFriend"■■■■■■■■■■■■■■■■
	
	public CustomerFriend friendDelete3(String email , String friendEmail) {
		
		SqlParameterSource param = new MapSqlParameterSource().addValue("email", email) 
															  .addValue("friendEmail", friendEmail) ;
		
		CustomerFriend csf;
		
		try {
			//レコードが存在しているかチェック　かつ　リターン用の情報をcsfに格納
			csf =jdbcTemplate.queryForObject(
					"SELECT * FROM `user_friend` WHERE Email =:email AND FriendEmail=:friendEmail"
					, param
					, customerFriendRowMapper);
			
			if( csf != null ) {//存在する場合
				//削除
				jdbcTemplate.update(
						"DELETE FROM `user_friend` WHERE Email = :email AND FriendEmail =:friendEmail "
						,param);
				
				return csf;
				
			}else {//存在しない場合
				return null;
			}
			
			
			
		}catch(EmptyResultDataAccessException e) {
			e.printStackTrace();
			System.out.println("そのアカウントはありません");
			return null;
		}
	}
	
	
	
	

}
