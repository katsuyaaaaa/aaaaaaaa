package com.example.repository;


import java.text.SimpleDateFormat;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;



import com.example.domain.*;  //ドメインパッケージを全てインポート
import java.util.ArrayList;   //アレイリストをインポート
import java.util.List;        //リストをインポート


@Repository
@Transactional


public class No6 {

	@Autowired
	NamedParameterJdbcTemplate jdbcTemplate;
	
	Customer c;
	
	private static final RowMapper<Customer> customerRowMapper = (rs,i) -> {
		String email = rs.getString("Email");
		String password = rs.getString("Password");
		String nickName = rs.getString("NickName");
		int point = rs.getInt("Point");
		int rank = rs.getInt("Rank");
		int age = rs.getInt("Age");
		String entryDate = new SimpleDateFormat("yyyy/MM/dd").format(rs.getDate("EntryDate"));
		String lastDateTime = new SimpleDateFormat("yyyy/MM/dd").format(rs.getDate("LastDateTime"));
		int loginState = rs.getInt("LoginState");
		
		return new Customer(email, password, nickName,point,rank,age,entryDate,lastDateTime,loginState);
	};
	
	
	
	private static final RowMapper<CustomerFriend> customerFriendRowMapper = (cf,i) -> {
		String email = cf.getString("Email");
		String friendemail = cf.getString("friendemail");
		
		
		return new CustomerFriend(email,friendemail);
	};
	
//■■■■■■■■■■■■■■■■■■■■６、フレンド一件登録　　GET型    URL"{email}/{friendEmail}/friend"■■■■■■■■■■■■■■■■■■■■
	
	public List<CustomerFriend> friendRegistration3(String email ,String friendEmail){
		
		List<CustomerFriend> friendList = new ArrayList<CustomerFriend>();
		
		Customer user;
		Customer friend;
		
		
		
		SqlParameterSource param = new MapSqlParameterSource().addValue("email", email) 
															  .addValue("friendEmail", friendEmail) ;
		try {
			//自分確認
			user = jdbcTemplate.queryForObject(
					"SELECT * FROM user_basic WHERE Email = :email "
					, param
					, customerRowMapper);
			//フレンド確認
			friend = jdbcTemplate.queryForObject(
					"SELECT * FROM user_basic WHERE Email = :friendEmail"
					, param
					, customerRowMapper);
			//フレンド５人未満か確認
			Integer friendMember =jdbcTemplate.queryForObject(
					"SELECT COUNT(*) FROM `user_friend` WHERE Email =:email"
					, param
					, Integer.class);
			
			
			//ユーザーとフレンドの確認
			if(user != null && friend != null && friendMember < 5 && !(email .equals(friendEmail)) ) {
				
				Integer overlap =jdbcTemplate.queryForObject(
						"SELECT COUNT(*) FROM `user_friend` WHERE Email =:email AND FriendEmail=:friendEmail"
						, param
						, Integer.class);
				
				
				//確認
				System.out.println(overlap);
				
				
				if( overlap == 0 ) {//登録されてない場合
					
					//登録
					jdbcTemplate.update(
							"INSERT INTO `user_friend`(`Email`, `FriendEmail`) VALUES (:email,:friendEmail)"
							,param);
					
					//friendListに自分のフレンドリストを入れる
					friendList = jdbcTemplate.query(
							"SELECT * FROM user_friend WHERE Email = :email"
							, param
							, customerFriendRowMapper);
					
					return friendList;
					
				}else {//すでに登録してる場合空のリストを返す
					System.out.println("空リスト");
					return friendList;
				}
				
			}else {//ユーザーとフレンドの確認
				return null;
			}
			
		}catch(EmptyResultDataAccessException e) {
			e.printStackTrace();
			System.out.println("そのアカウントはありません");
			return null;
		}
		
		
		
		
	}//friendRegistration3
	
	
	
}//class
