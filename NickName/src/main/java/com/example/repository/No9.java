package com.example.repository;

import java.text.SimpleDateFormat;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import ch.qos.logback.core.net.SyslogOutputStream;
import com.example.domain.Customer;

//■■■■■■■■■■■■■■■■■■■■■ポイント加算リポジトリクラス■■■■■■■■■■■■■■■■■■■■■■■■
@Repository
//クラッシュした場合に戻すことができる
@Transactional
public class No9 {
	
	
	//リポジトリ→サービス→コントローラー
	@Autowired
	//SQL文を使えるようにする
	NamedParameterJdbcTemplate jdbcTemplate;
	Customer c;
	int row;
	
	private static final RowMapper<Customer> ctrm = (rs,i) ->{
		
		String email = rs.getString("Email");
		String password = rs.getString("Password");
		String nickName = rs.getString("NickName");
		int point = rs.getInt("Point");
		int rank = rs.getInt("Rank");
		int age = rs.getInt("Age");
		String entryDate = new SimpleDateFormat("yyyy/MM/dd").format(rs.getDate("EntryDate"));
		String lastDateTime = new SimpleDateFormat("yyyy/MM/dd").format(rs.getDate("LastDateTime"));
		int loginState = rs.getInt("LoginState");
		
		return new Customer(email, password, nickName,point,rank,age,entryDate,lastDateTime,loginState);
	};
	

	//パラメーター定義
	public Customer userSearch3(String email,int point) {
		SqlParameterSource param = new MapSqlParameterSource().addValue("email",email)//kyeとvalueを定義
															  .addValue("point",point);
		try {
			
			c = jdbcTemplate.queryForObject(
					"SELECT * FROM user_basic WHERE Email = :email"
					, param
					, ctrm);
			
			row = jdbcTemplate.update("UPDATE user_basic SET Point = Point + :point WHERE Email = :email",param);
			System.out.println("ここまでエラーなし");
			System.out.println("row1=" + row);
			
			if(row == 0) {
				return null;	
			}else {
				return c;
			}
			
		}catch(EmptyResultDataAccessException e) {
			e.printStackTrace();
			System.out.println("そのアカウントはありません");
			return null;
		}	
	}
}
