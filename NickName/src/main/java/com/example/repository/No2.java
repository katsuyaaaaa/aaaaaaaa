package com.example.repository;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.jar.JarException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.example.domain.Customer;
import com.example.domain.CustomerFriend;
import com.example.domain.CustomerSave;
import com.mysql.jdbc.exceptions.jdbc4.MySQLIntegrityConstraintViolationException;

//■■■■■■■■■■■■■■■■■■■ユーザーフレンド全取得リポジトリ■■■■■■■■■■■■■■■■■■■■
@Repository
//エラー解決処理をしてくれる役割
@Transactional
public class No2 {
	@Autowired
	NamedParameterJdbcTemplate jdbcTemplate;
	
	Date date = new Date();
	String lastDateTime = new SimpleDateFormat("yyyy/MM/dd").format(date);
	String entrydate = new SimpleDateFormat("yyyy/MM/dd").format(date);
	CustomerSave c;

	
	private static final RowMapper<CustomerSave> ctmr = (rs,i) -> {
		String email = rs.getString("Email");
		String password = rs.getString("Password");
		String nickName = rs.getString("NickName");
		int age = rs.getInt("Age");

		
		return new CustomerSave(email, password, nickName,age);
	};


	public CustomerSave userRegistration3(String email,String password,String nickname,int age) {
		SqlParameterSource param = new MapSqlParameterSource().addValue("email",email)
														      .addValue("password",password)
														      .addValue("nickname",nickname)
														      .addValue("point",0)//
														      .addValue("rank", 0)//
														      .addValue("age",age)
														      .addValue("entrydate", entrydate)
		                                                      .addValue("lastDateTime",lastDateTime)
															  .addValue("loginstate",0);//
		try {
		int i =jdbcTemplate.update("INSERT INTO `user_basic`(`Email`, `Password`, `NickName`, `Point`, `Rank`, `Age`, `EntryDate`, `LastDateTime`, `LoginState`) "
	               + "VALUES (:email,:password,:nickname,:point,:rank,:age,:entrydate,:lastDateTime,:loginstate)"
	,param);
//		if(i==0) {
//			return null;
//		}
//		 c = jdbcTemplate.queryForObject(
//				"INSERT INTO `user_basic`(`Email`, `Password`, `NickName`, `Point`, `Rank`, `Age`, `EntryDate`, `LastDateTime`, `LoginState`) "
//				               + "VALUES (:email,:password,:nickname,:point,:rank,:age,:entrydate,:lastDateTime,:loginstate))"
//				,param
//				,ctmr);
		c=jdbcTemplate.queryForObject("SELECT `Email`,`Password`,`NickName`,`Age` FROM `user_basic` WHERE Email=:email", param, ctmr);
		 
	    

		return  c;	
		
	}catch(EmptyResultDataAccessException e) {
		e.printStackTrace();
		System.out.println("そのアカウントはありません");
		return null;
	}catch(DataAccessException e) {
		return null;
	}
  }
}