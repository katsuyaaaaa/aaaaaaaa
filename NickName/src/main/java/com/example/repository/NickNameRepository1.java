package com.example.repository;

import java.text.SimpleDateFormat;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.example.domain.Customer;

@Repository
@Transactional

public class NickNameRepository1 {
	
	@Autowired
	NamedParameterJdbcTemplate jdbcTemplate;
	
	Customer c;
	
	private static final RowMapper<Customer> customerRowMapper = (rs,i) -> {
		String email = rs.getString("Email");
		String password = rs.getString("Password");
		String nickName = rs.getString("NickName");
		int point = rs.getInt("Point");
		int rank = rs.getInt("Rank");
		int age = rs.getInt("Age");
		String entryDate = new SimpleDateFormat("yyyy/MM/dd").format(rs.getDate("EntryDate"));
		String lastDateTime = new SimpleDateFormat("yyyy/MM/dd").format(rs.getDate("LastDateTime"));
		int loginState = rs.getInt("LoginState");
		
		return new Customer(email, password, nickName,point,rank,age,entryDate,lastDateTime,loginState);
	};
	
	
	
	
//■■■■■■■■■■■■■■■■■■■■１、ユーザー情報一件取得　　GET型　　URL "{email}/find"■■■■■■■■■■■■■■■
	public Customer userSearch3(String email) {
		SqlParameterSource param = new MapSqlParameterSource().addValue("email", email) ;
		
		try {
//			jdbcTemplate.update("UPDATE user_basic SET NickName = :nickName WHERE :email", param);
			
			c = jdbcTemplate.queryForObject(
					"SELECT * FROM user_basic WHERE Email = :email"
					, param
					, customerRowMapper);
			
			return c;
			
		}catch(EmptyResultDataAccessException e) {
			e.printStackTrace();
			System.out.println("そのアカウントはありません");
			return null;
		}
	}//1

}