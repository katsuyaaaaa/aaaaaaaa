package com.example.repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.example.domain.Customer;

@Repository
@Transactional

public class NickNameRepository2 {
	
	@Autowired
	NamedParameterJdbcTemplate jdbcTemplate;

	Customer c;
	
	public Customer changeNickName(String email,String nickName) {
		SqlParameterSource param = new MapSqlParameterSource().addValue("email", email)
															  .addValue("nickName", nickName);
		
		try {
			jdbcTemplate.update("UPDATE user_basic SET NickName = :nickName WHERE :email", param);
			
			c = jdbcTemplate.queryForObject(
					"SELECT * FROM user_basic WHERE Email = :email"
					, param
					, new RowMapper<Customer>() {
						@Override
						public Customer mapRow(ResultSet rs,int rowNum) throws SQLException{
							return new Customer(rs.getString("Email"), rs.getString("Password"),
									rs.getString("NickName"), rs.getInt("Point"), 
									rs.getInt("Rank"),rs.getInt("Age"), 
									new SimpleDateFormat("yyyy/MM/dd").format( rs.getDate("EntryDate")), 
									new SimpleDateFormat("yyyy/MM/dd").format( rs.getDate("LastDateTime")),
									rs.getInt("LoginState"));
						}
							
					});
			return c;
		}catch(EmptyResultDataAccessException e) {
			e.printStackTrace();
			System.out.println("そのアカウントはありません");
			return null;
		}
	}
}