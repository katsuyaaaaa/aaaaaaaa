package com.example.repository;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.example.domain.Customer;
import com.example.domain.CustomerLogInAndLogOut;

@Repository
@Transactional

public class No4 {
	@Autowired
	NamedParameterJdbcTemplate jdbcTemplate;

	CustomerLogInAndLogOut c;

	private static final RowMapper<CustomerLogInAndLogOut> customerRowMapper = (rs, i) -> {
		String email = rs.getString("Email");
		String password = rs.getString("Password");
		String nickName = rs.getString("NickName");
		String lastDateTime = new SimpleDateFormat("yyyy/MM/dd").format(rs.getDate("LastDateTime"));
		int loginState = rs.getInt("LoginState");

		return new CustomerLogInAndLogOut(email, password, nickName, lastDateTime, loginState);
	};

	// ■■■■■■■■■■■■■■■■■■■■4、ログアウト POST型 URL "/logout"■■■■■■■■■■■■■■■
	public CustomerLogInAndLogOut logout3(String email,String password) {
		
		Date date = new Date();	
		String lastDateTime = new SimpleDateFormat("yyyy/MM/dd").format(date);
		
		SqlParameterSource param = new MapSqlParameterSource().addValue("email", email).addValue("lastDateTime",lastDateTime );
		try {

			c = jdbcTemplate.queryForObject("SELECT * FROM user_basic WHERE Email = :email", param, customerRowMapper);

			if (c.getLoginState() == 1) {
				jdbcTemplate.update("UPDATE user_basic SET loginState = 0 ,LastDateTime = :lastDateTime WHERE Email = :email", param);
				c = jdbcTemplate.queryForObject("SELECT * FROM user_basic WHERE Email = :email", param,
						customerRowMapper);
			} else if (c.getLoginState() == 0) {
				System.out.println("ログアウトずみ");
				c = null;
			} else {
				System.out.println("不正な状態です");
				c = null;
			}

			return c;

		} catch (EmptyResultDataAccessException e) {
			e.printStackTrace();
			System.out.println("そのアカウントはありません");
			return null;
		}
	}
}
