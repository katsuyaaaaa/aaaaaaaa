package com.example.repository;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.example.domain.Customer;
import com.example.domain.CustomerFriend;

//■■■■■■■■■■■■■■■■■■■ユーザーフレンド全取得リポジトリ■■■■■■■■■■■■■■■■■■■■
@Repository
//エラー解決処理をしてくれる役割
@Transactional
public class No5 {
	@Autowired
	NamedParameterJdbcTemplate jdbcTemplate;
	Customer c;
	
	List<CustomerFriend> friend;
	
	private static final RowMapper<Customer> ctmr = (rs,i) -> {
		String email = rs.getString("Email");
		String password = rs.getString("Password");
		String nickName = rs.getString("NickName");
		int point = rs.getInt("Point");
		int rank = rs.getInt("Rank");
		int age = rs.getInt("Age");
		String entryDate = new SimpleDateFormat("yyyy/MM/dd").format(rs.getDate("EntryDate"));
		String lastDateTime = new SimpleDateFormat("yyyy/MM/dd").format(rs.getDate("LastDateTime"));
		int loginState = rs.getInt("LoginState");
		
		return new Customer(email, password, nickName,point,rank,age,entryDate,lastDateTime,loginState);
	};
	
	private static final RowMapper<CustomerFriend> a = (rs,i) ->{
		String email = rs.getString("Email");
		String friendEmail =rs.getString("friendEmail");
		return new CustomerFriend(email,friendEmail);
	};
	public List<CustomerFriend> friendSearch3(String email) {
		
		List<CustomerFriend> fl = new ArrayList<CustomerFriend>();
		SqlParameterSource param = new MapSqlParameterSource().addValue("email",email);
		try {
			c = jdbcTemplate.queryForObject(
					"SELECT * FROM user_basic WHERE Email = :email"
					, param
					, ctmr);
			
			friend = jdbcTemplate.query("SELECT * FROM user_friend WHERE Email = :email",param,a);
			
			return friend;
	
			
		}catch(EmptyResultDataAccessException e) {
			e.printStackTrace();
			System.out.println("そのアカウントはありません");
			return null;
		}
	}
}
