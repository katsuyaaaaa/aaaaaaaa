﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

[System.Serializable]
public class FujiRegistration : MonoBehaviour {

    //URL
    string registrationUrl;
    string friendFindUrl;
    string friendInformationURL1;
    string friendInformationURL2;
    string friendInformationURL3;
    string friendInformationURL4;
    string friendInformationURL5;
    string friendDeleteURL;

    

    public Customer friendCustomer1;
    public Customer friendCustomer2;
    public Customer friendCustomer3;
    public Customer friendCustomer4;
    public Customer friendCustomer5;

    //入力されたフレンドのアドレス
    public Text friendEmailText;
    public string friendEmail;

    public string email;
    public string nickname;

    public Text userNameText;
    public Text userEmailText;
    public Text messageText;

    public Text nickNameText1;
    public Text nickNameText2;
    public Text nickNameText3;
    public Text nickNameText4;
    public Text nickNameText5;

    public Text emailText1;
    public Text emailText2;
    public Text emailText3;
    public Text emailText4;
    public Text emailText5;

    public Text dateText1;
    public Text dateText2;
    public Text dateText3;
    public Text dateText4;
    public Text dateText5;


    //ドメイン
    private CustomerFriendList customerFriendList;
    private CustomerFriend customerFriend;

    string[] cFriendList;

    private int ListNum;
    private int selectMonsterNum;

    private bool coroutineFlag;
    private bool findFlag;
    private bool clickFlag;


    private AudioSource sound01;
    private AudioSource sound02;
    private AudioSource sound03;
    private AudioSource sound04;
    private AudioSource sound05;
    private AudioSource sound06;

    public Animator animator;

    void Start ()
    {

        email = PlayerPrefs.GetString("email");
        nickname = PlayerPrefs.GetString("nickname");

        userNameText.text = nickname + "さん";
        userEmailText.text = "(" + email + ")";


        messageText.text = "";

        coroutineFlag = false;
        findFlag = false;

        cFriendList = new string[5];

        textNullSystem();
        

        friendFindUrl = "http://localhost:8080/customerapi/"+ email +"/friend";
        // 検索コルーチン
        StartCoroutine(friendFindCoroutine());

        clickFlag = true; //エラー防止

        selectMonsterNum = 0;

        AudioSource[] audioSources = GetComponents<AudioSource>();
        sound01 = audioSources[1];
        sound02 = audioSources[2];
        sound03 = audioSources[3];
        sound04 = audioSources[4];
        sound05 = audioSources[5];
        sound06 = audioSources[6];

    }//Start

    private void Update()
    {
        if (findFlag)
        {
            StartCoroutine(friendFindCoroutine());
            findFlag = false;
        }
        if (coroutineFlag)
        {
            friendInformationSystem();
            coroutineFlag = false;
        }

        

    }//Update

    //登録メソッド
    public void friendRegistrationClick()
    {

        Debug.Log("配列数" + ListNum);

          

        friendEmail = friendEmailText.text;

        if (email == friendEmail)
        {
            messageText.text = "自身のメールアドレスはフレンドとして登録できません。";
            sound03.Play();
        }
        else if (ListNum >= 5  )
        {
            messageText.text = "フレンドを5人以上登録することができません。";
            sound03.Play();
        }
        else if (friendEmail =="")
        {
            messageText.text = "未記入の部分があります";
            sound03.Play();
        }
        else if(email != friendEmail && ListNum < 5 && friendEmail != "" && clickFlag)//自分登録
        {
            
            registrationUrl = "http://localhost:8080/customerapi/" + email + "/" + friendEmail + "/friend";
            StartCoroutine(registrationCoroutine());//クリックしたらコルーチンスタート
            clickFlag = false;
            Debug.Log("クリックフラグ＝フォルス");
            
        }
    }//friendRegistrationClick

    //■■■■■■■■■■■■■■セレクト■■■■■■■■■■■■■■■■■■■
    
    public void friend1SelectClick()
    {
        if (ListNum >= 1)
        {
            PlayerPrefs.SetString("friendEmail", friendCustomer1.email);
            messageText.text = "ロビーに"+ friendCustomer1.nickname + "さんを反映しました。";
            selectMonsterNum = 1;
            sound01.Play();
        }
        else
        {
            sound04.Play();
        }
    }

    public void friend2SelectClick()
    {
        if (ListNum >= 2)
        {
            PlayerPrefs.SetString("friendEmail", friendCustomer2.email);
            messageText.text = "ロビーに" + friendCustomer2.nickname + "さんを反映しました。";
            selectMonsterNum = 2;
            sound01.Play();
        }
        else
        {
            sound04.Play();
        }
    }

    public void friend3SelectClick()
    {
        if (ListNum >= 3)
        {
            PlayerPrefs.SetString("friendEmail", friendCustomer3.email);
            messageText.text = "ロビーに" + friendCustomer3.nickname + "さんを反映しました。";
            selectMonsterNum = 3;
            sound01.Play();
        }
        else
        {
            sound04.Play();
        }
    }

    public void friend4SelectClick()
    {
        if (ListNum >= 4)
        {
            PlayerPrefs.SetString("friendEmail", friendCustomer4.email);
            messageText.text = "ロビーに" + friendCustomer4.nickname + "さんを反映しました。";
            selectMonsterNum = 4;
            sound01.Play();
        }
        else
        {
            sound04.Play();
        }
    }

    public void friend5SelectClick()
    {
        if (ListNum >= 5)
        {
            PlayerPrefs.SetString("friendEmail", friendCustomer5.email);
            messageText.text = "ロビーに" + friendCustomer5.nickname + "さんを反映しました。";
            selectMonsterNum = 5;
            sound01.Play();
        }
        else
        {
            sound04.Play();
        }
    }

    //■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■



    //■■■■■■■■■■■■■■■■■消去■■■■■■■■■■■■■■■■■■

    public void friend1DeleteClick()//NO１
    {
        if (ListNum >=1 && clickFlag)
        {
            friendDeleteURL = "http://localhost:8080/customerapi/" + email + "/" + friendCustomer1.email + "/deleteFriend";
            messageText.text = friendCustomer1.nickname + "さんを消去しました。";
            StartCoroutine(friendDeleteSystem());
            clickFlag = false;
            Debug.Log("クリックフラグ＝フォルス");

            FlashEffect.Play();
            sound05.Play();
            animator.SetTrigger("DleteAnim1");
            animator.SetTrigger("Friend1");
        }
        else
        {
            sound04.Play();
        }

        if (selectMonsterNum == 1)
        {
            PlayerPrefs.SetString("friendEmail", null);

        }
    }

    public void friend2DeleteClick()//NO２
    {
        if (ListNum >= 2 && clickFlag)
        {
            friendDeleteURL = "http://localhost:8080/customerapi/" + email + "/" + friendCustomer2.email + "/deleteFriend";
            messageText.text = friendCustomer2.nickname + "さんを消去しました。";
            StartCoroutine(friendDeleteSystem());
            clickFlag = false;
            Debug.Log("クリックフラグ＝フォルス");
            sound05.Play();
            animator.SetTrigger("DleteAnim1");
            animator.SetTrigger("Friend2");
            FlashEffect.Play();
        }
        else
        {
            sound04.Play();
        }
        if (selectMonsterNum == 2)
        {
            PlayerPrefs.SetString("friendEmail", null);
        }
    }

    public void friend3DeleteClick()//NO３
    {
        if (ListNum >= 3 && clickFlag)
        {
            friendDeleteURL = "http://localhost:8080/customerapi/" + email + "/" + friendCustomer3.email + "/deleteFriend";
            messageText.text = friendCustomer3.nickname + "さんを消去しました。";
            StartCoroutine(friendDeleteSystem());
            clickFlag = false;
            Debug.Log("クリックフラグ＝フォルス");
            sound05.Play();
            FlashEffect.Play();
            animator.SetTrigger("DleteAnim1");
            animator.SetTrigger("Friend3");
        }
        else
        {
            sound04.Play();
        }
        if (selectMonsterNum == 3)
        {
            PlayerPrefs.SetString("friendEmail", null);
        }
    }

    public void friend4DeleteClick()//NO４
    {
        if (ListNum >= 4 && clickFlag)
        {
            friendDeleteURL = "http://localhost:8080/customerapi/" + email + "/" + friendCustomer4.email + "/deleteFriend";
            messageText.text = friendCustomer4.nickname + "さんを消去しました。";
            StartCoroutine(friendDeleteSystem());
            clickFlag = false;
            Debug.Log("クリックフラグ＝フォルス");
            sound05.Play();
            FlashEffect.Play();
            animator.SetTrigger("DleteAnim1");
            animator.SetTrigger("Friend4");

        }
        else
        {
            sound04.Play();
        }
        if (selectMonsterNum == 4)
        {
            PlayerPrefs.SetString("friendEmail", null);
        }
    }

    public void friend5DeleteClick()//NO５

    {
        if (ListNum >= 5 && clickFlag)
        {
            friendDeleteURL = "http://localhost:8080/customerapi/" + email + "/" + friendCustomer5.email + "/deleteFriend";
            messageText.text = friendCustomer5.nickname + "さんを消去しました。";
            StartCoroutine(friendDeleteSystem());
            clickFlag = false;
            Debug.Log("クリックフラグ＝フォルス");
            sound05.Play();
            FlashEffect.Play();
            animator.SetTrigger("DleteAnim1");
            animator.SetTrigger("Friend5");
        }
        else
        {
            sound04.Play();
        }
        if (selectMonsterNum == 5)
        {
            PlayerPrefs.SetString("friendEmail", null);
        }
    }

    //■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■


        



    //登録コルーチン
    private IEnumerator registrationCoroutine()//コルーチンのメソッド名(変更して下さい)
    {

        //↓ドメイン変数
        //string jsonLine = JsonUtility.ToJson(customerFriendList);
        //byte[] jByte = System.Text.Encoding.UTF8.GetBytes(jsonLine); 
        Dictionary<string, string> dic = new Dictionary<string, string>();
        dic.Add("Content-Type", "application/json;charset=utf-8");
        WWW www = new WWW(registrationUrl);    //GET
      //WWW www = new WWW(url, jByte, dic);   //POST
        yield return www; //処理が終わるまで待つ



        Debug.Log("登録後中身"+www.text);//登録後中身
        

        if (www.text == "[]")//登録済の場合
        {
            messageText.text = "指定のユーザー はすでにフレンドに登録されています。";
            sound03.Play();
        }
        else if (www.text == "")//アドレスがリストにない場合
        {
            messageText.text = "ユーザーが存在しません。";
            sound03.Play();
        }
        else
        {
            messageText.text = "登録OK";
            ListNum++;
            sound01.Play();

        }

        findFlag = true;

    }//登録コルーチン


    //フレンド消去
    private IEnumerator friendDeleteSystem()
    {
        yield return new WaitForSeconds(4.5f);
        sound06.Play();
        yield return new WaitForSeconds(3.5f);

        //↓ドメイン変数
        //string jsonLine = JsonUtility.ToJson(customerFriend);
        //byte[] jByte = System.Text.Encoding.UTF8.GetBytes(jsonLine);
        Dictionary<string, string> dic = new Dictionary<string, string>();
        dic.Add("Content-Type", "application/json;charset=utf-8");
        WWW www = new WWW(friendDeleteURL);    //GET
                                               //WWW www = new WWW(url, jByte, dic);   //POST
        yield return www; //処理が終わるまで待つ

        Debug.Log("消去後中身" + www.text);//検索後中身

        customerFriend = JsonUtility.FromJson<CustomerFriend>(www.text);

        Debug.Log("消去オブジェクト中身" + customerFriend.email);

        findFlag = true;
    }

    //フレンド検索コルーチン
    private IEnumerator friendFindCoroutine()
    {

        //↓ドメイン変数
        //string jsonLine = JsonUtility.ToJson(customerFriendList);
        //byte[] jByte = System.Text.Encoding.UTF8.GetBytes(jsonLine);
        Dictionary<string, string> dic = new Dictionary<string, string>();
        dic.Add("Content-Type", "application/json;charset=utf-8");
        WWW www = new WWW(friendFindUrl);    //GET
      //WWW www = new WWW(url, jByte, dic);   //POST
        yield return www; //処理が終わるまで待つ

        
        
        Debug.Log("検索後中身"+www.text);//検索後中身

        //不必要な文字を消去
        string tempText = www.text;
        tempText = tempText.Replace("[{\"email\":", "");
        tempText = tempText.Replace("friendEmail","");
        tempText = tempText.Replace("\"", "");
        tempText = tempText.Replace("email", "");
        tempText = tempText.Replace("{", "");
        tempText = tempText.Replace("}", "");
        tempText = tempText.Replace("]", "");
        tempText = tempText.Replace(",", "");

        //　:　ごとに配列に格納
        string[] test = tempText.Split(':');


        ListNum = 0;//index数初期化
        textNullSystem();//フレンドリストテキスト初期化

        //配列のindexを５に調整
        for (int i = 0; i < test.Length; i++)
        {
            if ( i % 2 == 1)
            {
                Debug.Log("配列index"+ i/2);
                cFriendList[i/2] = test[i];
                Debug.Log("フレンド名"+cFriendList[i/2]);
                ListNum++;//配列の数を別のintで管理
                
            }
        }

        coroutineFlag = true;

    }//検索コルーチン

    


    //■■■■■■■■■■■■■■■■フレンド情報検索コルーチン■■■■■■■■■■■■■■■■
    //１
    private IEnumerator friendInformationCoroutine1()
    {
        //↓ドメイン変数
        //string jsonLine = JsonUtility.ToJson(friendCustomer1);
        //byte[] jByte = System.Text.Encoding.UTF8.GetBytes(jsonLine);
        Dictionary<string, string> dic = new Dictionary<string, string>();
        dic.Add("Content-Type", "application/json;charset=utf-8");
        WWW www = new WWW(friendInformationURL1);    //GET
      //WWW www = new WWW(url, jByte, dic);   //POST
        yield return www; //処理が終わるまで待つ
        
        Debug.Log("フレンド情報１検索後中身" + www.text);//検索後中身

        friendCustomer1 = JsonUtility.FromJson<Customer>(www.text);

        Debug.Log("オブジェクト１中身" + friendCustomer1.email);

        nickNameText1.text = friendCustomer1.nickname;
        emailText1.text = friendCustomer1.email;
        dateText1.text = friendCustomer1.lastDateTime;

        clickFlag = true; //エラー防止
        Debug.Log("クリックフラグ＝トゥルー");

    }//１

    //２
    private IEnumerator friendInformationCoroutine2()
    {
        //↓ドメイン変数
        //string jsonLine = JsonUtility.ToJson(friendCustomer2);
        //byte[] jByte = System.Text.Encoding.UTF8.GetBytes(jsonLine);
        Dictionary<string, string> dic = new Dictionary<string, string>();
        dic.Add("Content-Type", "application/json;charset=utf-8");
        WWW www = new WWW(friendInformationURL2);    //GET
      //WWW www = new WWW(url, jByte, dic);   //POST
        yield return www; //処理が終わるまで待つ

        Debug.Log("フレンド情報２検索後中身" + www.text);//検索後中身

        friendCustomer2 = JsonUtility.FromJson<Customer>(www.text);

        Debug.Log("オブジェクト２中身" + friendCustomer2.email);

        nickNameText2.text = friendCustomer2.nickname;
        emailText2.text = friendCustomer2.email;
        dateText2.text = friendCustomer2.lastDateTime;

    }//２

    //３
    private IEnumerator friendInformationCoroutine3()
    {
        //↓ドメイン変数
        //string jsonLine = JsonUtility.ToJson(friendCustomer3);
        //byte[] jByte = System.Text.Encoding.UTF8.GetBytes(jsonLine);
        Dictionary<string, string> dic = new Dictionary<string, string>();
        dic.Add("Content-Type", "application/json;charset=utf-8");
        WWW www = new WWW(friendInformationURL3);    //GET
      //WWW www = new WWW(url, jByte, dic);   //POST
        yield return www; //処理が終わるまで待つ

        Debug.Log("フレンド情報３検索後中身" + www.text);//検索後中身

        friendCustomer3 = JsonUtility.FromJson<Customer>(www.text);

        Debug.Log("オブジェクト３中身" + friendCustomer3.email);

        nickNameText3.text = friendCustomer3.nickname;
        emailText3.text = friendCustomer3.email;
        dateText3.text = friendCustomer3.lastDateTime;

    }//３

    //４
    private IEnumerator friendInformationCoroutine4()
    {
        //↓ドメイン変数
        //string jsonLine = JsonUtility.ToJson(friendCustomer4);
        //byte[] jByte = System.Text.Encoding.UTF8.GetBytes(jsonLine);
        Dictionary<string, string> dic = new Dictionary<string, string>();
        dic.Add("Content-Type", "application/json;charset=utf-8");
        WWW www = new WWW(friendInformationURL4);    //GET
      //WWW www = new WWW(url, jByte, dic);   //POST
        yield return www; //処理が終わるまで待つ

        Debug.Log("フレンド情報４検索後中身" + www.text);//検索後中身

        friendCustomer4 = JsonUtility.FromJson<Customer>(www.text);

        Debug.Log("オブジェクト４中身" + friendCustomer4.email);

        nickNameText4.text = friendCustomer4.nickname;
        emailText4.text = friendCustomer4.email;
        dateText4.text = friendCustomer4.lastDateTime;

    }//４

    //５
    private IEnumerator friendInformationCoroutine5()
    {
        //↓ドメイン変数
        //string jsonLine = JsonUtility.ToJson(friendCustomer5);
        //byte[] jByte = System.Text.Encoding.UTF8.GetBytes(jsonLine);
        Dictionary<string, string> dic = new Dictionary<string, string>();
        dic.Add("Content-Type", "application/json;charset=utf-8");
        WWW www = new WWW(friendInformationURL5);    //GET
      //WWW www = new WWW(url, jByte, dic);   //POST
        yield return www; //処理が終わるまで待つ

        Debug.Log("フレンド情報５検索後中身" + www.text);//検索後中身

        friendCustomer5 = JsonUtility.FromJson<Customer>(www.text);

        Debug.Log("オブジェクト５中身" + friendCustomer5.email);

        nickNameText5.text = friendCustomer5.nickname;
        emailText5.text = friendCustomer5.email;
        dateText5.text = friendCustomer5.lastDateTime;

    }//５

    //■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■

    
    //フレンド情報検索メソッド
    public void friendInformationSystem()
    {

        //for (int i = 1 ; i <= ListNum; i++)
        //{
        //    friendInformationURL1 = "http://localhost:8080/customerapi/" + cFriendList[i-1] + "/find";
        //    StartCoroutine(friendInformationCoroutine1());
        //}



        switch (ListNum)
        {
            case 0:
                clickFlag = true;
                break;
            case 1:
                friendInformationURL1 = "http://localhost:8080/customerapi/" + cFriendList[0] + "/find";
                StartCoroutine(friendInformationCoroutine1());
                break;
            case 2:
                friendInformationURL1 = "http://localhost:8080/customerapi/" + cFriendList[0] + "/find";
                StartCoroutine(friendInformationCoroutine1());
                friendInformationURL2 = "http://localhost:8080/customerapi/" + cFriendList[1] + "/find";
                StartCoroutine(friendInformationCoroutine2());
                break;
            case 3:
                friendInformationURL1 = "http://localhost:8080/customerapi/" + cFriendList[0] + "/find";
                StartCoroutine(friendInformationCoroutine1());
                friendInformationURL2 = "http://localhost:8080/customerapi/" + cFriendList[1] + "/find";
                StartCoroutine(friendInformationCoroutine2());
                friendInformationURL3 = "http://localhost:8080/customerapi/" + cFriendList[2] + "/find";
                StartCoroutine(friendInformationCoroutine3());
                break;
            case 4:
                friendInformationURL1 = "http://localhost:8080/customerapi/" + cFriendList[0] + "/find";
                StartCoroutine(friendInformationCoroutine1());
                friendInformationURL2 = "http://localhost:8080/customerapi/" + cFriendList[1] + "/find";
                StartCoroutine(friendInformationCoroutine2());
                friendInformationURL3 = "http://localhost:8080/customerapi/" + cFriendList[2] + "/find";
                StartCoroutine(friendInformationCoroutine3());
                friendInformationURL4 = "http://localhost:8080/customerapi/" + cFriendList[3] + "/find";
                StartCoroutine(friendInformationCoroutine4());
                break;
            case 5:
                friendInformationURL1 = "http://localhost:8080/customerapi/" + cFriendList[0] + "/find";
                StartCoroutine(friendInformationCoroutine1());
                friendInformationURL2 = "http://localhost:8080/customerapi/" + cFriendList[1] + "/find";
                StartCoroutine(friendInformationCoroutine2());
                friendInformationURL3 = "http://localhost:8080/customerapi/" + cFriendList[2] + "/find";
                StartCoroutine(friendInformationCoroutine3());
                friendInformationURL4 = "http://localhost:8080/customerapi/" + cFriendList[3] + "/find";
                StartCoroutine(friendInformationCoroutine4());
                friendInformationURL5 = "http://localhost:8080/customerapi/" + cFriendList[4] + "/find";
                StartCoroutine(friendInformationCoroutine5());
                break;
        }
    }



    //文字を空にするメソッド
    public void textNullSystem()
    {
        nickNameText1.text = "";
        nickNameText2.text = "";
        nickNameText3.text = "";
        nickNameText4.text = "";
        nickNameText5.text = "";

        emailText1.text = "";
        emailText2.text = "";
        emailText3.text = "";
        emailText4.text = "";
        emailText5.text = "";

        dateText1.text = "";
        dateText2.text = "";
        dateText3.text = "";
        dateText4.text = "";
        dateText5.text = "";
    }
}
