﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class CustomerFriendList  {

    public CustomerFriend[] friendList;
}
