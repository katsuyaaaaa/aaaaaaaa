﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class CustomerLogInAndLogOut {

    public string email;
    public string password;
    public string nickname;
    public string lastDateTime;
    public int loginState;

   
}
