﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

//[System.Serializable]
public class YamaRegistration : MonoBehaviour {

    //URL
    string url;

    //CustomerSave型
    CustomerSave c =new CustomerSave();
    //SaveScript型
    // SaveScript saveScript;

    //入力されたもの
    public InputField email;
    public InputField password;
    public InputField nickName;
    public InputField age;
    public Text text;

    public Text allnull;//どれか入力されてない
    public Text noage;//半角数字のみ使用
    public Text ok;//登録完了

    public bool e;
    public bool p;
    public bool n;
    public bool a;

     void Start() {
 
        //どれか入力されていない
        allnull.GetComponent<Text>().enabled = false;
        //半角か否か
        noage.GetComponent<Text>().enabled =false;
        //登録ok
        ok.GetComponent<Text>().enabled = false;

    }

    public void click()//クリックしたときのメソッド(変更して下さい)
    {

        //もしemailテキストが空白とnullではない場合
        if (email.text != "" && email.text != null) {
            //CustomerSaveのc.emailにテキストを入れる
            c.email = email.text;
            Debug.Log(c.email = email.text);
            e = true;
        } else {
            //テキスト表示
            allnull.GetComponent<Text>().enabled = true;        
        }

        //パスワード
        if (password.text != "" && password.text != null) {
            c.password = password.text;
            Debug.Log(c.password = password.text);
            p = true;
        } else {
            //テキスト表示
            allnull.GetComponent<Text>().enabled = true;
        }

        //ニックネーム
        if (nickName.text != "" && nickName.text != null) {
            c.nickname = nickName.text;
            Debug.Log(c.nickname = nickName.text);
            n = true;
        } else {
            //テキスト表示
            allnull.GetComponent<Text>().enabled = true;
        }

        //年齢
        if (age.text != "" && age.text != null) {
            c.age = int.Parse(age.text);
            Debug.Log(c.age = int.Parse(age.text));
            a = true;
        } else {
            //テキスト表示
            noage.GetComponent<Text>().enabled = true;
            allnull.GetComponent<Text>().enabled = true;
        }
     
        url = "http://localhost:8080/customerapi/save";//URLを記入
        StartCoroutine(Save());//クリックしたらコルーチンスタート
    }


    //↓コルーチン
    private IEnumerator Save()//コルーチンのメソッド名(変更して下さい)
    {
        //↓ドメイン変数
        string jsonLine = JsonUtility.ToJson(c);

        byte[] jByte = System.Text.Encoding.UTF8.GetBytes(jsonLine);
        Dictionary<string, string> dic = new Dictionary<string, string>();

        dic.Add("Content-Type", "application/json;charset=utf-8");


       // WWW www = new WWW(url);    //GET
       WWW www = new WWW(url, jByte, dic);   //POST

        yield return www; //処理が終わるまで待つ

        Debug.Log(www.text);//確認


        //↓ドメイン変数　　　　　　↓ドメインのクラス
        c = JsonUtility.FromJson<CustomerSave>(www.text);

        Debug.Log(c);//確認


        //全部登録完了だった場合
        if (e && p && n && a == true) {
            ok.GetComponent<Text>().enabled = true;
        }

        if (c == null) {
            ok.text = @"記入されたメールアドレスは
すでに登録されています。";
        }else if (c != null) {
            yield return new WaitForSeconds(2);
            SceneManager.LoadScene("KatsuyaScene");
        }
    }
}
