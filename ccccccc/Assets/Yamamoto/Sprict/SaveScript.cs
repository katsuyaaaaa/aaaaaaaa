﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SaveScript : MonoBehaviour {

    //CustomerSaveの情報を入れる
    CustomerSave c = new CustomerSave();
    public string email1;
    public string pass1;
    public string nickName1;
    public string age1;

    //inputしてもらったデータ
    public InputField email;
    public InputField password;
    public InputField nickName;
    public InputField age;
    public Text text;

    public void SaveText() {
        //入力してもらったデータをCustomerSave型に入れる　
        if (email.text != "" && email.text != null) {
            c.email = email.text;
            Debug.Log(c.email);

            email1 = "" + email;
        }
        if (password.text != "" && password.text != null) {
            c.password = password.text;
            Debug.Log(c.password = password.text);

            pass1 = "" + password;
        }
        if (nickName.text != "" && nickName.text != null) {
            c.nickname = nickName.text;
            Debug.Log(c.nickname = nickName.text);

            nickName1 = "" + nickName;
        }
        if (age.text != "" && age.text != null) {
            c.age = int.Parse(age.text);
            Debug.Log(c.age = int.Parse(age.text));

            age1 = "" + age;
        }
    }
}
