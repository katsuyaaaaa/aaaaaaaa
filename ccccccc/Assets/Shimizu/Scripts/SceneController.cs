﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneController : MonoBehaviour
{

    string url;

    CustomerLogInAndLogOut cl;
    public static bool flag;





    // ボタンをクリックするとBattleSceneに移動します
    public void FriendButtonClicked()
    {
        SceneManager.LoadScene("FriendScene");


    }
    public void LogoutButtonClicked()
    {

        cl = new CustomerLogInAndLogOut();
        cl.email = PlayerPrefs.GetString("email");
        cl.password = PlayerPrefs.GetString("password");
        url = "http://localhost:8080/customerapi/logout";
        StartCoroutine(koru(cl));
        SceneManager.LoadScene("KatsuyaScene");//勝谷さんのログイン画面に遷移

    }

    private IEnumerator koru(CustomerLogInAndLogOut cl)
    {
        //↓ドメイン変数
        string jsonLine = JsonUtility.ToJson(cl);

        byte[] jByte = System.Text.Encoding.UTF8.GetBytes(jsonLine);
        Dictionary<string, string> dic = new Dictionary<string, string>();

        dic.Add("Content-Type", "application/json;charset=utf-8");

        WWW www = new WWW(url, jByte, dic);    //GET
                                               //WWW www = new WWW(url, jByte, dic);   //POST

        yield return www; //処理が終わるまで待つ

        Debug.Log(www.text);//確認

        //↓ドメイン変数　　　　　　↓ドメインのクラス
        cl = JsonUtility.FromJson<CustomerLogInAndLogOut>(www.text);


        PlayerPrefs.DeleteKey("email");
        PlayerPrefs.DeleteKey("password");

        Debug.Log(PlayerPrefs.GetString("email"));
        
        PlayerPrefs.DeleteKey("nickname");
        PlayerPrefs.DeleteKey("point");
        PlayerPrefs.DeleteKey("rank");
        PlayerPrefs.DeleteKey("age");
        PlayerPrefs.DeleteKey("entryDate");
        PlayerPrefs.DeleteKey("lastDateTime");
        PlayerPrefs.DeleteKey("loginState");
      

    }


}

