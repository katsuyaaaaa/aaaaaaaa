﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ImageController : MonoBehaviour
{



    //URL
    string url;
    string url2;
    string url3;

    //ドメイン
    Monster m;//ドメインをインスタンス化
    CustomerFriend cf;
    Customer c;

    public Sprite elf;
    public Sprite slime;
    public Sprite bee;
    public Sprite devil;
    public Sprite dragon;

    private Image UserM1;
    private Image UserM2;
    private Image UserM3;
    private Image UserM4;
    private Image UserM5;

    private Image FriendM1;
    private Image FriendM2;
    private Image FriendM3;
    private Image FriendM4;
    private Image FriendM5;

    public Text Username;
    public Text UserMonstertext;

    public Text Friendname;
    public Text FriendMonstertext;




    void Start()
    {


        //                                        ↓URLに変数を入れる
        url = "http://localhost:8080/customerapi/" + PlayerPrefs.GetString("email") + "/monster";

            url2 = "http://localhost:8080/customerapi/" + PlayerPrefs.GetString("friendEmail") + "/monster";

        //URLを記入
        StartCoroutine(koru());
        StartCoroutine(koru2());

    }

    //↓ユーザー情報とユーザーモンスター情報のコルーチン
    private IEnumerator koru()//コルーチンのメソッド名(変更して下さい)
    {
        //↓ドメイン変数
        // string jsonLine = JsonUtility.ToJson(m);

        //byte[] jByte = System.Text.Encoding.UTF8.GetBytes(jsonLine);
        //Dictionary<string, string> dic = new Dictionary<string, string>();

        //dic.Add("Content-Type", "application/json;charset=utf-8");

        WWW www = new WWW(url);    //GET
                                   //WWW www = new WWW(url, jByte, dic);   //POST

        yield return www; //処理が終わるまで待つ

        //Debug.Log(www.text);//確認

        //↓ドメイン変数　　　　　　↓ドメインのクラス
        //m = JsonUtility.FromJson<Monster>(www.text);


        string[] monst = www.text.Split(',');


        Username.text = PlayerPrefs.GetString("nickname") + "さん、こんにちは";
        UserMonstertext.text = PlayerPrefs.GetString("nickname") + "さん、所持モンスター";

        for (int i = 0; i < monst.Length; i++)
        {
            switch (monst[i])
            {
                case "elf":
                    UserM1 = GameObject.Find("UserM1").GetComponent<Image>();
                    UserM1.sprite = elf;
                    break;
                case "slime":
                    UserM2 = GameObject.Find("UserM2").GetComponent<Image>();
                    UserM2.sprite = slime;
                    break;
                case "bee":
                    UserM3 = GameObject.Find("UserM3").GetComponent<Image>();
                    UserM3.sprite = bee;
                    break;
                case "devil":
                    UserM4 = GameObject.Find("UserM4").GetComponent<Image>();
                    UserM4.sprite = devil;
                    break;
                case "dragon":
                    UserM5 = GameObject.Find("UserM5").GetComponent<Image>();
                    UserM5.sprite = dragon;
                    break;
            }

        }


    }
    private IEnumerator koru2()//フレンドのモンスターを表示するコルーチン
    {
        //↓ドメイン変数
        // string jsonLine = JsonUtility.ToJson(m);

        //byte[] jByte = System.Text.Encoding.UTF8.GetBytes(jsonLine);
        //Dictionary<string, string> dic = new Dictionary<string, string>();

        //dic.Add("Content-Type", "application/json;charset=utf-8");

        WWW www = new WWW(url2);    //GET
                                    //WWW www = new WWW(url, jByte, dic);   //POST

        yield return www; //処理が終わるまで待つ

        //Debug.Log(www.text);//確認

        //↓ドメイン変数　　　　　　↓ドメインのクラス
        //m = JsonUtility.FromJson<Monster>(www.text);


        string[] monst = www.text.Split(',');




        for (int i = 0; i < monst.Length; i++)
        {
            switch (monst[i])
            {
                case "elf":
                    FriendM1 = GameObject.Find("FriendM1").GetComponent<Image>();
                    FriendM1.sprite = elf;
                    break;
                case "slime":
                    FriendM2 = GameObject.Find("FriendM2").GetComponent<Image>();
                    FriendM2.sprite = slime;
                    break;
                case "bee":
                    FriendM3 = GameObject.Find("FriendM3").GetComponent<Image>();
                    FriendM3.sprite = bee;
                    break;
                case "devil":
                    FriendM4 = GameObject.Find("FriendM4").GetComponent<Image>();
                    FriendM4.sprite = devil;
                    break;
                case "dragon":
                    FriendM5 = GameObject.Find("FriendM5").GetComponent<Image>();
                    FriendM5.sprite = dragon;
                    break;
            }


        }
      
            Friendnamae();
       
    }


    public void Friendnamae()
    {
        c = new Customer();
        c.email = PlayerPrefs.GetString("friendEmail");
        url3 = "http://localhost:8080/customerapi/" + c.email + "/find";


        StartCoroutine(koru3(c));

    }

    private IEnumerator koru3(Customer c)//コルーチンのメソッド名(変更して下さい)
    {
        //↓ドメイン変数
        // string jsonLine = JsonUtility.ToJson(m);

        //byte[] jByte = System.Text.Encoding.UTF8.GetBytes(jsonLine);
        //Dictionary<string, string> dic = new Dictionary<string, string>();

        //dic.Add("Content-Type", "application/json;charset=utf-8");

        WWW www = new WWW(url3);    //GET
                                    //WWW www = new WWW(url, jByte, dic);   //POST

        yield return www; //処理が終わるまで待つ

        //Debug.Log(www.text);//確認

        //↓ドメイン変数　　　　　　↓ドメインのクラス
        c = JsonUtility.FromJson<Customer>(www.text);

        Debug.Log(c.nickname);

        FriendMonstertext.text = c.nickname + "さん、所持モンスター";

        PlayerPrefs.DeleteKey("friendEmail");
        c = null;





    }
}


