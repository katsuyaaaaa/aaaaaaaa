﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class DB : MonoBehaviour
{

    //URL
    string url;

    //ドメイン
    Customer c;//ドメインをインスタンス化


    void Start()
    {
        //                                      ↓URLに変数を入れる
        url = "http://localhost:8080/customerapi/aa@zz.com/find";//URLを記入

    }

    public void Lv1Button()//クリックしたときのメソッド(変更して下さい)
    {
        StartCoroutine(koru());//クリックしたらコルーチンスタート
    }


    //↓コルーチン
    private IEnumerator koru()//コルーチンのメソッド名(変更して下さい)
    {
        //↓ドメイン変数
        //string jsonLine = JsonUtility.ToJson(c);

        //byte[] jByte = System.Text.Encoding.UTF8.GetBytes(jsonLine);
        //Dictionary<string, string> dic = new Dictionary<string, string>();

        //dic.Add("Content-Type", "application/json;charset=utf-8");


        WWW www = new WWW(url);    //GET
                                   //WWW www = new WWW(url, jByte, dic);   //POST

        yield return www; //処理が終わるまで待つ

        Debug.Log(www.text);//確認


        //↓ドメイン変数　　　　　　↓ドメインのクラス
        c = JsonUtility.FromJson<Customer>(www.text);

        Debug.Log(c.email);//確認
    }



}
