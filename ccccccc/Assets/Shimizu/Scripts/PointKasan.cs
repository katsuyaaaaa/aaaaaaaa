﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PointKasan : MonoBehaviour
{
    string url;
    int num = 0;

    Customer c;

    public Text PointText;


    private void Start()
    {
        PointText.text = "現在のポイントは" + PlayerPrefs.GetInt("point") + "です。";
    }




    public void Lv1ButtonClicked()
    {
        num = 100;
        c = new Customer();
        url = "http://localhost:8080/customerapi/" + PlayerPrefs.GetString("email") + "/" + num + "/point";
        
        StartCoroutine(Point (c));
    }
    public void Lv2ButtonClicked()
    {
        num = 200;
        c = new Customer();
        url = "http://localhost:8080/customerapi/" + PlayerPrefs.GetString("email") + "/" + num + "/point";

        StartCoroutine(Point(c));
    }
    public void Lv3ButtonClicked()
    {
        num = 300;
        c = new Customer();
        url = "http://localhost:8080/customerapi/" + PlayerPrefs.GetString("email") + "/" + num + "/point";

        StartCoroutine(Point(c));
    }

    private IEnumerator Point(Customer c)
    {
       string jsonLine = JsonUtility.ToJson(c);

        WWW www = new WWW(url);

        yield return www;

        c = JsonUtility.FromJson<Customer>(www.text);
        int result = c.point + num;
        PointText.text = "現在のポイントは" + result + "です。";
       


        // PointText.text = "現在のポイントは" + (c.point + point) + "です";
        num = 0;
    }




}


