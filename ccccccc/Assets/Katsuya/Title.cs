﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

[System.Serializable]
public class Title : MonoBehaviour {

    public InputField mail; //emailのインプットフィールド
    public InputField pw; //passwordのインプットフィールド
    public Text message;
    public Text mailtext;
    public Text pwtext;
    public GameObject nowLoding;
    public GameObject loginb;
    public GameObject ca;
    public GameObject lo;
    public AudioClip sound;
    
    //URL
    string url;

    //ドメイン
    Customer customer;
    CustomerLogInAndLogOut c;//ドメインをインスタンス化
    LoginUser lu;

    // Use this for initialization
    void Start () {
        loginb.SetActive(true);
        ca.SetActive(true);
        lo.SetActive(false);
        //mail = GetComponent<InputField>();
        //pw = GetComponent<InputField>();
        //mailtext = GetComponent<Text>();
        //pwtext = GetComponent<Text>();
        //message = GetComponent<Text>();
        //mailtext.text = "aaa";
        //Debug.Log(mailtext.text);
    }

    // Update is called once per frame
    void Update () {
       

    }

    public void OnClick()
    {
        nowLoding.SetActive(true);
        loginb.SetActive(false);
        ca.SetActive(false);
        AudioSource.PlayClipAtPoint(sound, transform.position);
        Debug.Log(mailtext.text);
        //Debug.Log(pwtext);

        
        if ((mailtext.text == "" || mailtext.text == null) && (pwtext.text != ""&& pwtext.text != null))
        {
            message.text = "Emailを入力してください";
            nowLoding.SetActive(false);
            loginb.SetActive(true);
            ca.SetActive(true);
        }
        else if ((pwtext.text == "" || pwtext.text == null) && (mailtext.text != "" && mailtext.text != null))
        {
            message.text = "Passwordを入力してください";
            nowLoding.SetActive(false);
            loginb.SetActive(true);
            ca.SetActive(true);
        }
        else if (mailtext.text == "" || pwtext.text == "" || mailtext.text == null || pwtext.text == null)
        {
            //処理を止める
            message.text = "EmailとPasswordを入力してください";
            nowLoding.SetActive(false);
            loginb.SetActive(true);
            ca.SetActive(true);
        }
        else
        {
        //カスタマー型で情報取得
        customer = new Customer();
        customer.email = mail.text;
        url = "http://localhost:8080/customerapi/"+customer.email+"/find";

            Debug.Log("Customer");
            Debug.Log(url);
        
        StartCoroutine(findc(customer));
            
        }
    }
    public void Log() {
        //CustomerLogInAndLogOutをインスタンス化
        c = new CustomerLogInAndLogOut();
        c.email = mailtext.text;
        c.password = pwtext.text;
        Debug.Log(c.email);

        //                                      ↓URLに変数を入れる
        url = "http://localhost:8080/customerapi/login";//URLを記入

        Debug.Log("Login");
        Debug.Log(url);
        
        StartCoroutine(login(c));
    }
    public void OnCreate()
    {
        AudioSource.PlayClipAtPoint(sound, transform.position);
        Debug.Log("create");
        SceneManager.LoadScene("NewUsar");
    }

    //カスタマー型のコルーチン
    private IEnumerator findc(Customer customer)//コルーチンのメソッド名(変更して下さい)
    {
        Debug.Log(url);
        //↓ドメイン変数
        string jsonLine = JsonUtility.ToJson(customer);

        WWW www = new WWW(url);    //GET
       
        yield return www; //処理が終わるまで待つ

        Debug.Log(www.text);//確認


        //↓ドメイン変数　　　　　　↓ドメインのクラス
        customer = JsonUtility.FromJson<Customer>(www.text);
        
        if (customer!=null && customer.loginState == 1)
        {
            message.text = "ログイン中です";
            nowLoding.SetActive(false);
            loginb.SetActive(true);
            ca.SetActive(true);
            lo.SetActive(true);
        }
        else if (customer == null)
        {
            message.text = "ユーザーアカウントが存在しません";
            nowLoding.SetActive(false);
            loginb.SetActive(true);
            ca.SetActive(true);
        }
        else if(customer.loginState==0)
        {
            //ログイン処理
            Log();
        }
        else
        {
            message.text = "エラー";
            nowLoding.SetActive(false);
            loginb.SetActive(true);
            ca.SetActive(true);
        }
    }

        //↓コルーチン
        private IEnumerator login(CustomerLogInAndLogOut c)//コルーチンのメソッド名(変更して下さい)
    {
        Debug.Log(url);
        //↓ドメイン変数
        string jsonLine = JsonUtility.ToJson(c);

        byte[] jByte = System.Text.Encoding.UTF8.GetBytes(jsonLine);
        Dictionary<string, string> dic = new Dictionary<string, string>();

        dic.Add("Content-Type", "application/json;charset=utf-8");


        //WWW www = new WWW(url);    //GET
        WWW www = new WWW(url, jByte, dic);   //POST

        yield return www; //処理が終わるまで待つ

        Debug.Log(www.text);//確認


        //↓ドメイン変数　　　　　　↓ドメインのクラス
        c = JsonUtility.FromJson<CustomerLogInAndLogOut>(www.text);
        
        Debug.Log(customer.loginState);

        
        if (c == null)
        {
            message.text = "ユーザーアカウントが存在しません";
            nowLoding.SetActive(false);
            loginb.SetActive(true);
            ca.SetActive(true);
        }
        else if (c.loginState == 1 &&  c != null)
        {
            LoginU();
        }
        else
        {
            message.text = "エラー";
            nowLoding.SetActive(false);
            loginb.SetActive(true);
            ca.SetActive(true);
        }
       
    }

    public void LoginU()
    {
        lu = new LoginUser();
        lu.email = mail.text;
        Debug.Log("ログイン成功");

        //                                      ↓URLに変数を入れる
        url = "http://localhost:8080/customerapi/" + lu.email + "/loginuser";//URLを記入

        StartCoroutine(loginUser(lu)); 
    }

    private IEnumerator loginUser(LoginUser lu)//コルーチンのメソッ
        //↓ドメイン変数ド名(変更して下さい)
    {
        //string jsonLine = JsonUtility.ToJson(c);

        //byte[] jByte = System.Text.Encoding.UTF8.GetBytes(jsonLine);
        //Dictionary<string, string> dic = new Dictionary<string, string>();

        //dic.Add("Content-Type", "application/json;charset=utf-8");


        WWW www = new WWW(url);    //GET
        //WWW www = new WWW(url, jByte, dic);   //POST

        yield return www; //処理が終わるまで待つ

        Debug.Log(www.text);//確認

        //↓ドメイン変数　　　　　　↓ドメインのクラス
        lu = JsonUtility.FromJson<LoginUser>(www.text);

        Debug.Log(www.text);
       
        //PlayerPrefs.Set + 保存する変数の型（文字列のキー, 保存したい値）
        //public string email;
        //public string password;
        //public string nickname;
        //public int point;
        //public int rank;
        //public int age;
        //public string entryDate;
        //public string lastDateTime;
        //public int loginState;
        PlayerPrefs.SetString("email", lu.email);
        PlayerPrefs.SetString("password", lu.password);
        PlayerPrefs.SetString("nickname", lu.nickname);
        PlayerPrefs.SetInt("point", lu.point);
        PlayerPrefs.SetInt("rank", lu.rank);
        PlayerPrefs.SetInt("age", lu.age);
        PlayerPrefs.SetString("entryDate", lu.entryDate);
        PlayerPrefs.SetString("lastDateTime", lu.lastDateTime);
        PlayerPrefs.SetInt("loginState", lu.loginState);

        nowLoding.SetActive(false);
        
        SceneManager.LoadScene("UserLobby");
        //SceneManager.LoadScene("TestScene");
    }
    public void OnClickLogout()
    {
        lo.SetActive(false);
        AudioSource.PlayClipAtPoint(sound, transform.position);
        Debug.Log(mailtext.text);
        //Debug.Log(pwtext);


        if ((mailtext.text == "" || mailtext.text == null) && (pwtext.text != "" && pwtext.text != null))
        {
            message.text = "Emailを入力してください";
            nowLoding.SetActive(false);
            loginb.SetActive(true);
            ca.SetActive(true);
            lo.SetActive(false);
        }
        else if ((pwtext.text == "" || pwtext.text == null) && (mailtext.text != "" && mailtext.text != null))
        {
            message.text = "Passwordを入力してください";
            nowLoding.SetActive(false);
            loginb.SetActive(true);
            ca.SetActive(true);
            lo.SetActive(false);
        }
        else if (mailtext.text == "" || pwtext.text == "" || mailtext.text == null || pwtext.text == null)
        {
            //処理を止める
            message.text = "EmailとPasswordを入力してください";
            nowLoding.SetActive(false);
            loginb.SetActive(true);
            ca.SetActive(true);
            lo.SetActive(false);
        }
        else
        {
            //カスタマー型で情報取得
            customer = new Customer();
            customer.email = mail.text;
            customer.password = pw.text;
            url = "http://localhost:8080/customerapi/" + customer.email + "/find";

            Debug.Log("Customer");
            Debug.Log(url);

            StartCoroutine(findd(customer));

        }
    }

    //カスタマー型のコルーチン
    private IEnumerator findd(Customer customer)//コルーチンのメソッド名(変更して下さい)
    {
        Debug.Log(url);
        //↓ドメイン変数
        string jsonLine = JsonUtility.ToJson(customer);

        WWW www = new WWW(url);    //GET

        yield return www; //処理が終わるまで待つ

        Debug.Log(www.text);//確認


        //↓ドメイン変数　　　　　　↓ドメインのクラス
        customer = JsonUtility.FromJson<Customer>(www.text);

       
        if (customer == null)
        {
            message.text = "ユーザーアカウントが存在しません";
            nowLoding.SetActive(false);
            loginb.SetActive(true);
            ca.SetActive(true);
            lo.SetActive(false);
        }
        else if (customer.loginState == 1 && customer.email==mail.text && customer.password==pw.text)
        {
            //ログイン処理
            Logoutout();
        }
        else if (customer.password != pw.text)
        {
            message.text = "Passwodが間違っています";
        }
        else
        {
            message.text = "エラー";
            nowLoding.SetActive(false);
            loginb.SetActive(true);
            ca.SetActive(true);
            lo.SetActive(false);

        }
    }
    public void Logoutout()
    {
        //CustomerLogInAndLogOutをインスタンス化
        c = new CustomerLogInAndLogOut();
        c.email = mailtext.text;
        c.password = pwtext.text;

        //                                      ↓URLに変数を入れる
        url = "http://localhost:8080/customerapi/logout";//URLを記入

        StartCoroutine(logout(c));
    }
    private IEnumerator logout(CustomerLogInAndLogOut logOut)
    {
        message.text = "ログアウトしました";
        Debug.Log("Logiout");
        Debug.Log(url);
        //↓ドメイン変数
        string jsonLine = JsonUtility.ToJson(c);

        byte[] jByte = System.Text.Encoding.UTF8.GetBytes(jsonLine);
        Dictionary<string, string> dic = new Dictionary<string, string>();

        dic.Add("Content-Type", "application/json;charset=utf-8");


        //WWW www = new WWW(url);    //GET
        WWW www = new WWW(url, jByte, dic);   //POST

        yield return www; //処理が終わるまで待つ

        Debug.Log(www.text);//確認


        //↓ドメイン変数　　　　　　↓ドメインのクラス
        c = JsonUtility.FromJson<CustomerLogInAndLogOut>(www.text);
    }
}