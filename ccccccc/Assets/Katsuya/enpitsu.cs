﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class enpitsu : MonoBehaviour {


    Rigidbody2D rb;
    public float moveSpeed = 1.0f;
    public AudioClip sound;


    // Use this for initialization
    void Start () {
        rb = GetComponent<Rigidbody2D>();
    }
	
	// Update is called once per frame
	void Update () {
        Vector3 scale = transform.localScale;
        if (scale.x==1)
        {
            rb.velocity = new Vector2(-moveSpeed, rb.velocity.y);
        }
        else if (scale.x == -1)
        {
            rb.velocity = new Vector2(moveSpeed, rb.velocity.y);
        }
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
      
        AudioSource.PlayClipAtPoint(sound, transform.position);
        Vector3 scale = transform.localScale;
        if (scale.x==1)
        {
            scale.x = -1;
        }
        else if (scale.x==-1)
        {
            scale.x = 1;
        }
        transform.localScale = scale;
    }
}
