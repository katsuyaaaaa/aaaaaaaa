﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PointPoint : MonoBehaviour {
    public Text text;
    //URL
    string url;
    //加算ポイント
    int point=0;
    //ドメイン
    Customer c;

	// Use this for initialization
	void Start () {
        text.text = "あなたのポイントは" + PlayerPrefs.GetInt("point") + "です";
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void OnClickLv1()
    {
        point = 100;
        //カスタマー型で情報取得
        c = new Customer();
        Debug.Log("Point=" + point);
        //{email}/{point}/point 
        url = "http://localhost:8080/customerapi/" + PlayerPrefs.GetString("email") + "/"+point+"/point";

        //Debug.Log(c.point);
        //Debug.Log(url);

        StartCoroutine(Point(c));
    }

    public void OnClickLv2()
    {
        point = 200;
        //カスタマー型で情報取得
        c = new Customer();
        Debug.Log("Point=" + point);

        //{email}/{point}/point 
        url = "http://localhost:8080/customerapi/" + PlayerPrefs.GetString("email") + "/" + point + "/point";

        Debug.Log(c.point);
        Debug.Log(url);

        StartCoroutine(Point(c));


    }

    public void OnClickLv3()
    {
        point = 300;
        //カスタマー型で情報取得
        c = new Customer();
        Debug.Log("Point=" + point);

        //{email}/{point}/point 
        url = "http://localhost:8080/customerapi/" + PlayerPrefs.GetString("email") + "/" + point + "/point";

        Debug.Log(c.point);
        Debug.Log(url);

        StartCoroutine(Point(c));


    }

    private IEnumerator Point(Customer c)//コルーチンのメソッド名(変更して下さい)
    {
        Debug.Log(url);
        //↓ドメイン変数
        //string jsonLine = JsonUtility.ToJson(c);

        WWW www = new WWW(url);//GET

        yield return www; //処理が終わるまで待つ

        Debug.Log(www.text);//確認


        //↓ドメイン変数　　　　　　↓ドメインのクラス
        c = JsonUtility.FromJson<Customer>(www.text);
        int result =c.point+point;
        text.text = "あなたのポイントは" + result + "です";
        point = 0;
    }
}
