﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class LoginUser {

    public string email;
    public string password;
    public string nickname;
    public int point;
    public int rank;
    public int age;
    public string entryDate;
    public string lastDateTime;
    public int loginState;

}
